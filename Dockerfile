FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/spring-microservices-2.0.0.RELEASE.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]